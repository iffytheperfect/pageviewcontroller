//
//  MyPageViewController.swift
//  PageControllerTry
//
//  Created by Phanit Pollavith on 10/16/18.
//  Copyright © 2018 iffytheperfect. All rights reserved.
//

import UIKit

class MyPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
  var controllers = [UIViewController]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    dataSource = self
    delegate = self
    
    for _ in 1 ... 5 {
      let vc = UIViewController()
      vc.view.backgroundColor = randomColor()
      let fakeView = UIView(frame: .init(x: 100, y: 100, width: 100, height: 100))
      fakeView.backgroundColor = randomColor()
      vc.view.addSubview(fakeView)
      controllers.append(vc)
    }
    
    setViewControllers([controllers[0]], direction: .forward, animated: false)
  }
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    if let index = controllers.index(of: viewController) {
      if index > 0 {
        return controllers[index - 1]
      } else {
        return nil
      }
    }
    
    return nil
  }
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    if let index = controllers.index(of: viewController) {
      if index < controllers.count - 1 {
        return controllers[index + 1]
      } else {
        return nil
      }
    }
    
    return nil
  }
  
  func randomCGFloat() -> CGFloat {
    return CGFloat(arc4random()) / CGFloat(UInt32.max)
  }
  
  func randomColor() -> UIColor {
    return UIColor(red: randomCGFloat(), green: randomCGFloat(), blue: randomCGFloat(), alpha: 1)
  }
}
